import java.util.Scanner;

public class Ex6 {
	static Scanner scn = new Scanner(System.in);

	public static void main(String[] args) {

		int numpos=0;
		int numneg=0;
		int numCero=0;
		int num;
		
		for(int i=0;i<10;i++) {
			System.out.println("Introdueix numero " + (i+1)+":");
			num=scn.nextInt();	
				if(num>0){
					numpos++;
				}
				if(num<0){
					numneg++;
				}
				if(num==0){
					numCero++;
				}
		}
		System.out.println("Hay " + numpos + " numeros positivos");
		System.out.println("Hay " + numneg + " numeros negativos");
		System.out.println("Hay " + numCero + " numeros que son 0");
				
	}

}