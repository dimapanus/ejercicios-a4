import java.util.Scanner;

public class Ex11 {
	static Scanner scn = new Scanner(System.in);

	public static void main(String[] args) {

		int num;
		int divi = 1;

		System.out.println("introdueix un numero: ");
		num = scn.nextInt();

		while (divi != num) {
			if (num % divi == 0) {
				System.out.println("El numero " + divi + " es divisor de " + num);
				divi++;
			} else {
				divi++;
			}

		}
	}
}